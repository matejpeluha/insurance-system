package sk.stuba.fei.oop.web.system.insurances.insuranceTypes;

import lombok.Getter;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public abstract class LifeInsurance  extends Insurance {
    @NotNull
    protected int insuredID;

    public LifeInsurance(){
        super();
    }

    @Override
    public boolean isNotInitialized() {
        return super.isNotInitialized();
    }

    @Override
    public String toString() {
        return super.toString() + "\ninsuredID: " + insuredID;
    }
}
