package sk.stuba.fei.oop.web.system.userInfo.details;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class Mail {
    @NotNull
    @Email
    private String mail;

    public boolean isInitialized(){
        return mail != null;
    }

    @Override
    public String toString() {
        return mail;
    }
}
