package sk.stuba.fei.oop.web.system.insurances.insuranceTypes;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.RealEstateType;
import sk.stuba.fei.oop.web.system.userInfo.details.Address;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
public abstract class NonLifeInsurance extends Insurance {
    @NotNull
    protected Address address;
    @Min(1000)
    protected Integer value;
    @NotNull
    protected RealEstateType realEstateType;

    public NonLifeInsurance(){
        super();
    }

    @Override
    public boolean isNotInitialized() {
        return super.isNotInitialized() || address == null  || realEstateType == null || !address.isInitialized();
    }

    @Override
    public String toString() {
        return super.toString() + "\n" + address + "\nvalue: " + value + "\n" + realEstateType;
    }
}
