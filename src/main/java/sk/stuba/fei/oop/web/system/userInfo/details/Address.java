package sk.stuba.fei.oop.web.system.userInfo.details;

import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Getter
public class Address {
    @NotBlank
    private String postCode;
    @NotBlank
    private String municipality;
    @NotBlank
    private String street;
    @Min(1)
    private Integer houseNumber;

    public boolean isInitialized(){
        return postCode != null && municipality != null && street != null && houseNumber != null &&
                !postCode.equals("") && !municipality.equals("") && !street.equals("") && houseNumber > 0;
    }

    public void setHouseNumber(Integer houseNumber){
        if(houseNumber == null)
            this.houseNumber = 1;
        else
            this.houseNumber = houseNumber;
    }


    @Override
    public String toString() {
        return street + " " + houseNumber + ", " + municipality + " " + postCode;
    }
}
