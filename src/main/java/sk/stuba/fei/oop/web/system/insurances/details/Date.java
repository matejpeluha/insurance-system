package sk.stuba.fei.oop.web.system.insurances.details;

import lombok.Getter;

import java.util.GregorianCalendar;

@Getter
public class Date {
    private int year;
    private int month;
    private int day;

    public Date(int day, int month, int year){
        this.year = year;
        setMonth(month);
        setDay(day);
    }

    private boolean isLeapYearFebruary(){
        GregorianCalendar calendar = new GregorianCalendar();

        return calendar.isLeapYear(year) && month == 2;
    }

    private boolean is30DaysMonth(){
        return month == 4 || month == 6 || month == 9 || month == 11;
    }

    private boolean is31DaysMonth(){
        return month == 1 || month == 3 || month == 5 || month ==7 || month == 8 || month == 10 || month == 12;
    }

    private void setMonth(int month)throws IllegalArgumentException{
        if(0 >= month || month > 12)
            throw new IllegalArgumentException("Neplatny datum");

        this.month = month;
    }

    private void setDay(int day)throws IllegalArgumentException{
        if(day <= 0)
            throw new IllegalArgumentException("Neplatny datum.");
        else if(is30DaysMonth() && day > 30)
            throw new IllegalArgumentException("Neplatny datum.");
        else if(is31DaysMonth() && day > 31)
            throw new IllegalArgumentException("Neplatny datum");
        else if(isLeapYearFebruary() && day > 29)
            throw new IllegalArgumentException("Neplatny datum");
        else if(month == 2 && day > 28)
            throw new IllegalArgumentException("Neplatny datum");

        this.day = day;
    }

    @Override
    public String toString() {
        return day + "." + month + "." + year;
    }
}
