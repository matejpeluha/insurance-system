package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum EuropeUnionArea {
    NON_EU,
    EU;

    //****** Reverse Lookup Implementation************//

    private static final Map<String, EuropeUnionArea> lookup = new HashMap<String, EuropeUnionArea>();

    static {
        for(EuropeUnionArea EUArea : EuropeUnionArea.values())
        {
            lookup.put(EUArea.name(), EUArea);
        }
    }

    public static EuropeUnionArea getEnum(String EUMembership) throws NullPointerException{
        if(EUMembership == null)
            throw new NullPointerException("Neexistujuci string.");

        return lookup.get(EUMembership.toUpperCase());
    }

    public static ArrayList<EuropeUnionArea> getEnumsWithStarting(EuropeUnionArea area) throws NullPointerException{
        if(area == null)
            throw new NullPointerException("Neexistujuci enum.");

        ArrayList<EuropeUnionArea> enums = new ArrayList<EuropeUnionArea>(Arrays.asList(values()));
        enums.remove(area);
        enums.add(0, area);

        return enums;
    }
}
