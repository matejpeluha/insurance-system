package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details;

import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.TravelInsurance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum TravelPurpose {
    WORK,
    VACATION,
    SPORT,
    FAMILY,
    OTHER;

    //****** Reverse Lookup Implementation************//

    private static final Map<String, TravelPurpose> lookup = new HashMap<String, TravelPurpose>();

    static {
        for(TravelPurpose travelPurpose : TravelPurpose.values())
        {
            lookup.put(travelPurpose.name(), travelPurpose);
        }
    }

    public static TravelPurpose getEnum(String travelPurpose) throws NullPointerException {
        if(travelPurpose == null)
            throw new NullPointerException("Neexistujuci string.");

        return lookup.get(travelPurpose.toUpperCase());
    }

    public static ArrayList<TravelPurpose> getEnumsWithStarting(TravelPurpose purpose) throws NullPointerException{
        if(purpose == null)
            throw new NullPointerException("Neexistujuci enum.");

        ArrayList<TravelPurpose> enums = new ArrayList<TravelPurpose>(Arrays.asList(values()));
        enums.remove(purpose);
        enums.add(0, purpose);

        return enums;
    }

}
