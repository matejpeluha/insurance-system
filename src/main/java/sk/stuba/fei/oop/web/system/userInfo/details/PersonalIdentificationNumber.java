package sk.stuba.fei.oop.web.system.userInfo.details;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Calendar;
import java.util.GregorianCalendar;


@Data
public class PersonalIdentificationNumber {
    @NotBlank
    private final String PIN;

    public PersonalIdentificationNumber(String pin)throws NullPointerException, IllegalArgumentException{
        this.PIN = pin;
    }

    private boolean isStringNumber(String string){
        for (char c : string.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    private boolean isValidPinFormat(){
        final int pinLength = PIN.length();

        if(pinLength != 10 && pinLength != 11)
            return false;
        else if(PIN.charAt(6) != '/')
            return false;

        String date = PIN.substring(0,6);
        String end = PIN.substring(7, pinLength);

        return isStringNumber(date) && isStringNumber(end);
    }

    private int getYear(String date, String end){
        String year;
        String yearStringShortcut = date.substring(0, 2);
        int yearShortcut = Integer.parseInt(yearStringShortcut);

        if(end.length() == 4 && yearShortcut < 54)
            year = "20" + yearStringShortcut;
        else if(end.length() == 3 && yearShortcut >= 54)
            year = "18" + yearStringShortcut;
        else
            year = "19" + yearStringShortcut;

        return Integer.parseInt(year);
    }

    private int getMonth(String date){
        String month = date.substring(2, 4);

        return Integer.parseInt(month);
    }

    private int getDay(String date){
        String day = date.substring(4, 6);

        return Integer.parseInt(day);
    }

    private boolean is31DaysMonth(int month){
        return month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12;
    }

    private boolean is30DaysMonth(int month){
        return month == 4 || month == 6 || month == 9 || month == 11;
    }

    private boolean isFebruary(int month){
        return month == 2;
    }

    private boolean isLeapYear(int year){
        GregorianCalendar calendar = new GregorianCalendar();

        return calendar.isLeapYear(year);
    }

    private boolean isDateFromPastThisYearAndMonth(int day){
        int currentDay = Calendar.getInstance().get(Calendar.DATE);

        return day < currentDay;
    }

    private boolean isDateFromPastThisYear(int day, int month){
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;

        if(month > currentMonth)
            return false;
        else if(month == currentMonth)
            return isDateFromPastThisYearAndMonth(day);
        return true;
    }

    private boolean isDateFromPast(int day, int month, int year){
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if(year > currentYear)
            return false;
        else if(year == currentYear)
            return isDateFromPastThisYear(day, month);

        return true;
    }

    private boolean isDateValid(int day, int month, int year){
        if(day <= 0)
            return false;
        if(is31DaysMonth(month) && day <= 31)
            return true;
        else if(is30DaysMonth(month) && day <= 30)
            return true;
        else if(isFebruary(month) && isLeapYear(year) && day <= 29 )
            return true;
        else if(isFebruary(month) && day <= 28 )
            return true;

        return false;
    }

    private boolean isDateValid(){
        String date = PIN.substring(0,6);
        String end = PIN.substring(7, PIN.length());

        int year = getYear(date, end);
        int month = getMonth(date);
        int day = getDay(date);

        return isDateValid(day, month, year) && isDateFromPast(day, month, year);
    }

    public boolean isValidPin(){
        if(PIN != null && isValidPinFormat())
            return isDateValid();

        return false;
    }


    @Override
    public String toString() {
        return PIN;
    }
}
