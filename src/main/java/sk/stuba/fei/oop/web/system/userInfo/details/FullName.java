package sk.stuba.fei.oop.web.system.userInfo.details;

import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotBlank;


@Data
@Getter
public class FullName {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;

    public boolean isInitialized(){
        return firstName!= null && lastName != null && !firstName.equals("") && !lastName.equals("");
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
