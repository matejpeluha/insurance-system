package sk.stuba.fei.oop.web.system;

import org.springframework.stereotype.Service;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.EuropeUnionArea;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.TravelPurpose;
import sk.stuba.fei.oop.web.system.userInfo.Person;
import sk.stuba.fei.oop.web.system.userInfo.details.Address;
import sk.stuba.fei.oop.web.system.userInfo.details.FullName;
import sk.stuba.fei.oop.web.system.userInfo.details.PersonalIdentificationNumber;

import java.time.LocalDate;

@Service
public class DemoInsuranceSystemCreator {

    public Person createTestPerson1(){
        Address address = new Address();
        address.setStreet("kralovska");
        address.setHouseNumber(15);
        address.setMunicipality("Bratislava");
        address.setPostCode("821 03");

        Person person = new Person(0);
        person.setPermanentResidenceAddress(address);
        person.setMail("tvojtatko@gmail.com");
        FullName name = new FullName();
        name.setFirstName("Matej");
        name.setLastName("Peluha");
        person.setName(name);
        person.setPIN("951012/1225");

        return person;
    }

    public Person createTestPerson2(){
        Address address = new Address();
        address.setStreet("Molecova");
        address.setHouseNumber(1);
        address.setMunicipality("Presov");
        address.setPostCode("826 66");

        Person person = new Person(1);
        person.setPermanentResidenceAddress(address);
        person.setMail("majster.sveta@gmail.com");
        FullName name = new FullName();
        name.setFirstName("Jozko");
        name.setLastName("Golonka");
        person.setName(name);
        person.setPIN("751019/1225");

        return person;
    }

    public Insurance createTestInsurance(Person person){
        TravelInsurance insurance = new TravelInsurance();

        insurance.setID(0);

        insurance.setDateOfConclusionContract(LocalDate.of(2020, 5, 1));
        insurance.setStartDateInsurance(LocalDate.of(2020, 5, 15));
        insurance.setEndDateInsurance(LocalDate.of(2020, 7, 1));
        insurance.setInsuranceIndemnity(20000);
        insurance.setMonthlyPayment(100);
        insurance.setInsuredID(person.getID());
        insurance.setAreaCoverage(EuropeUnionArea.getEnum("NON_EU"));
        insurance.setTravelPurpose(TravelPurpose.VACATION);

        insurance.setInsurerID(person.getID());

        return insurance;
    }

    public InsuranceSystem createSystemWithUser(){
        InsuranceSystem system = new InsuranceSystem();

        Person person = createTestPerson1();

            system.addPerson(person);

            Person person1 = system.getPerson(1);
            Insurance insurance = createTestInsurance(person1);
            system.addInsurance(insurance);


        return system;
    }
}
