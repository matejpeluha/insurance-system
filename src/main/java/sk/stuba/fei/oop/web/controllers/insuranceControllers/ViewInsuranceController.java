package sk.stuba.fei.oop.web.controllers.insuranceControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.LifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.HomeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.RealEstateInsurance;


@Controller
@RequestMapping("/insurance")
public class ViewInsuranceController {
    @Autowired
    protected InsuranceSystem system;

    protected void addAttributeToModel(Model model, Insurance insurance){
        if(insurance instanceof HomeInsurance)
            model.addAttribute("homeInsurance", (HomeInsurance)insurance);
        else if(insurance instanceof RealEstateInsurance)
            model.addAttribute("realEstateInsurance", (RealEstateInsurance)insurance);
        else if(insurance instanceof TravelInsurance)
            model.addAttribute("travelInsurance", (TravelInsurance)insurance);
        else if(insurance instanceof AccidentInsurance)
            model.addAttribute("accidentInsurance", (AccidentInsurance)insurance);
        if(insurance instanceof LifeInsurance)
            model.addAttribute("insured", system.getPerson(((LifeInsurance)insurance).getInsuredID()));
    }

    @GetMapping("/id/{id}")
    public ModelAndView byId(@PathVariable int id, Model model) {
        Insurance insurance = system.getInsurance(id);

        addAttributeToModel(model, insurance);
        model.addAttribute("insurer", system.getPerson(insurance.getInsurerID()));

        if(insurance instanceof TravelInsurance)
            return new ModelAndView("insurance/oneTravel");
        else if(insurance instanceof AccidentInsurance)
            return new ModelAndView("insurance/oneAccident");
        else if(insurance instanceof HomeInsurance)
            return new ModelAndView("insurance/oneHome");
        else if(insurance instanceof RealEstateInsurance)
            return new ModelAndView("insurance/oneRealEstate");

        return new ModelAndView("redirect:/user/");
    }

    @GetMapping("/remove/all/{id}")
    public ModelAndView removeFromAll(@PathVariable int id){
        try {
            system.removeInsurance(id);
        }catch (Exception e){
            return new ModelAndView("redirect:/insurance/");
        }

        return new ModelAndView("redirect:/insurance/");
    }

    @GetMapping("/fromUser/{idUser}/remove/{idInsurance}")
    public ModelAndView removeFromUser(@PathVariable int idInsurance, @PathVariable int idUser){
        try {
            system.removeInsurance(idInsurance);
        }catch (Exception e){
            return new ModelAndView("redirect:/user/id/" + idUser);
        }
        return new ModelAndView("redirect:/user/id/" + idUser);
    }

    @GetMapping("/")
    public ModelAndView findAll(Model model) {
        model.addAttribute("insurances", system.getAllInsurances());
        model.addAttribute("system", system);
        return new ModelAndView("insurance/all");
    }

}
