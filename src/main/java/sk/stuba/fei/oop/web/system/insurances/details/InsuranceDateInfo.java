package sk.stuba.fei.oop.web.system.insurances.details;

import lombok.Getter;

@Getter
public class InsuranceDateInfo {
    private Date dateOfConclusionContract;
    private Date startDateInsurance;
    private Date endDateInsurance;

    public void setDateOfConclusionContract(int day, int month, int year) throws IllegalArgumentException {
        this.dateOfConclusionContract = new Date(day, month, year);
    }

    private boolean isStartDayEarlierThanEndDay(){
        int startDay = startDateInsurance.getDay();
        int endDay = endDateInsurance.getDay();

        return startDay < endDay;
    }

    private boolean isStartMonthEarlierThanEndMonth(){
        int startMonth = startDateInsurance.getMonth();
        int endMonth = endDateInsurance.getMonth();

        if(startMonth > endMonth)
            return false;
        else if(startMonth == endMonth)
            return isStartDayEarlierThanEndDay();

        return true;
    }

    private boolean isStartDateEarlierThanEndDate(){
        int startYear = startDateInsurance.getYear();
        int endYear = endDateInsurance.getYear();

        if(startYear > endYear)
            return false;
        else if(startYear == endYear)
            return isStartMonthEarlierThanEndMonth();

        return true;
    }

    public void setStartDateInsurance(int day, int month, int year)throws IllegalArgumentException{
        this.startDateInsurance = new Date(day, month, year);

        if(endDateInsurance != null && !isStartDateEarlierThanEndDate()){
            this.startDateInsurance = null;
            throw new IllegalArgumentException("Zaciatocny datum je neskorsi ako koncovy.");
        }
    }

    public void setEndDateInsurance(int day, int month, int year)throws IllegalArgumentException{
        this.endDateInsurance = new Date(day, month, year);

        if(startDateInsurance!= null && !isStartDateEarlierThanEndDate()){
            this.endDateInsurance = null;
            throw new IllegalArgumentException("Koncovy datum je neskorsi ako zaciatocny.");
        }
    }

    public boolean areAllDatesInitialized(){
        return dateOfConclusionContract != null && startDateInsurance != null && endDateInsurance != null;
    }

    @Override
    public String toString() {
        return "conclusion: " + dateOfConclusionContract + "\nstart: " + startDateInsurance + "\nend: " + endDateInsurance;
    }
}
