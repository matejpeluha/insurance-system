package sk.stuba.fei.oop.web.system.userInfo;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.userInfo.details.Address;
import sk.stuba.fei.oop.web.system.userInfo.details.FullName;
import sk.stuba.fei.oop.web.system.userInfo.details.PersonalIdentificationNumber;

import javax.validation.constraints.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

@Data
@NoArgsConstructor
@Setter
@Getter
public class Person implements Comparable<Person>{
    @Min(0)
    private int ID;
    @NotNull
    private String PIN;
    @NotNull
    private FullName name;
    @Email
    @NotBlank
    private String mail;
    @NotNull
    private Address permanentResidenceAddress;
    @NotNull
    private Address correspondenceAddress;

    private ArrayList<Integer> allSignedInsurancesID = new ArrayList<Integer>();;

    public Person(int ID) throws IllegalArgumentException{
        final int MINIMAL_ID = 0;

        if(ID < MINIMAL_ID)
            throw new IllegalArgumentException("Neplatne ID");

        this.ID = ID;
    }

    private void sortAllInsuranceByID(){
        Collections.sort(this.allSignedInsurancesID);
    }

    public void addInsurance(Insurance insurance) throws  IllegalArgumentException, NullPointerException{
        if(insurance == null)
            throw new NullPointerException("Neexistujuce poistenie");
        else if(isInsuranceUsed(insurance))
            throw new IllegalArgumentException("Uzivatel ma tuto poistku zapisanu.");
        else if(insurance.isNotInitialized())
            throw new IllegalArgumentException("Zmluva nema inicializovane vsetky udaje.");


        this.allSignedInsurancesID.add(insurance.getID());
        sortAllInsuranceByID();
    }

    public boolean isInsuranceUsed(Insurance newInsurance)throws NullPointerException{
        if(newInsurance == null)
            throw new NullPointerException("Poistenie neexistuje.");

        for(int insuranceID : allSignedInsurancesID){
            if(insuranceID == newInsurance.getID())
                return true;
        }

        return false;
    }

    public boolean isNotInitialized(){
        return PIN == null || name == null || mail == null || permanentResidenceAddress == null || correspondenceAddress == null
                || !permanentResidenceAddress.isInitialized() || !correspondenceAddress.isInitialized() ||
                !name.isInitialized() || PIN == null;
    }

    public void setPermanentResidenceAddress(Address address)throws NullPointerException, IllegalComponentStateException {
        this.permanentResidenceAddress = address;

        if (correspondenceAddress == null)
            this.correspondenceAddress = address;
    }

    public void setFirstName(String firstName){
        this.name.setFirstName(firstName);
    }

    public void setLastName(String lastName){
        this.name.setLastName(lastName);
    }

    public Integer getInsuranceIntegerID(int id){
        for (Integer idInsurance : allSignedInsurancesID){
            if(id == idInsurance)
                return idInsurance;
        }

        throw new NullPointerException();
    }

    public void removeInsurance(int id){
        Integer idInsurance = getInsuranceIntegerID(id);
        allSignedInsurancesID.remove(idInsurance);
    }

    @Override
    public String toString() {
        return name.toString();
    }

    public int compareTo(Person o) {
        return Integer.compare(ID, o.getID());
    }
}
