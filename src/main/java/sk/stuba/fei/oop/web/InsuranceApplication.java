package sk.stuba.fei.oop.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sk.stuba.fei.oop.web.system.InsuranceSystem;


@Slf4j
@SpringBootApplication
public class InsuranceApplication implements CommandLineRunner {
    @Autowired
    private InsuranceSystem system;

    @Override
    public void run(String... args) throws Exception {
        system.addTestPeople();
        system.addTestInsurance();
    }


    public static void main(String[] args){
        SpringApplication.run(InsuranceApplication.class, args);
        log.info("Open in browser: http://localhost:8080");
    }
}
