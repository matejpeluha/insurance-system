package sk.stuba.fei.oop.web.system.insurances;

import lombok.Data;
import sk.stuba.fei.oop.web.system.userInfo.Person;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
public abstract class Insurance implements Comparable<Insurance>{
    @Min(0)
    protected int ID;
    @NotNull
    @Min(200)
    protected Integer insuranceIndemnity;
    @NotNull
    @Min(1)
    protected Integer monthlyPayment;
    protected int insurerID;
    @NotNull
    @PastOrPresent
    private LocalDate dateOfConclusionContract;
    @NotNull
    private LocalDate startDateInsurance;
    @NotNull
    @Future
    private LocalDate endDateInsurance;


    public boolean isNotInitialized(){
        return dateOfConclusionContract == null || startDateInsurance == null || endDateInsurance == null || insuranceIndemnity == null || monthlyPayment == null ;
    }

    @Override
    public String toString() {
        return ID + "\nsign:" + dateOfConclusionContract + "\nstart: " + startDateInsurance + "\nend: " + endDateInsurance + "\nindemnity: " + insuranceIndemnity + "\nmonthly: " + monthlyPayment + "\ninsurer: " + insurerID;
    }

    public int compareTo(Insurance insurance) {
        return Integer.compare(ID, insurance.getID());
    }
}
