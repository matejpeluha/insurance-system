package sk.stuba.fei.oop.web.controllers.insuranceControllers.changeControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.LifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.EuropeUnionArea;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.TravelPurpose;

@Controller
@RequestMapping("/insurance")
public class ChangeTravelController {
    @Autowired
    protected InsuranceSystem system;

    @GetMapping("/change/travel/id/{id}")
    public ModelAndView changeTravelForm(@PathVariable int id, Model model){
        Insurance insurance = system.getInsurance(id);
        model.addAttribute("travelInsurance", (TravelInsurance)insurance);
        model.addAttribute("insured", system.getPerson(((LifeInsurance)insurance).getInsuredID()));
        model.addAttribute("allAreaCoverages", EuropeUnionArea.getEnumsWithStarting(((TravelInsurance) insurance).getAreaCoverage()));
        model.addAttribute("allTravelPurposes", TravelPurpose.getEnumsWithStarting(((TravelInsurance) insurance).getTravelPurpose()));
        return new ModelAndView("insurance/change/changeTravel");
    }

    @PostMapping("/change/travel/{id}")
    public ModelAndView changeTravelSubmit(@PathVariable int id, @ModelAttribute TravelInsurance insurance, Model model, BindingResult bindingResult){
        insurance.setID(id);
        insurance = (TravelInsurance) system.setUnchangedParametersLifeInsurance(insurance);

        if(bindingResult.hasErrors())
            return changeTravelForm(id, model);

        if(insurance.isNotInitialized())
            return changeTravelForm(id, model);

        system.changeInsurance(insurance);

        return new ModelAndView("redirect:/insurance/id/{id}");
    }


}
