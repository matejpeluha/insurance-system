package sk.stuba.fei.oop.web.controllers.insuranceControllers.addControllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.RealEstateInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.Answer;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.RealEstateType;

import javax.validation.Valid;

@Controller
@RequestMapping("/insurance")
public class AddRealEstateController extends AddInsuranceController{

    @GetMapping("/add/realEstate/toUser/{idUser}")
    public ModelAndView addRealEstateForm(@PathVariable int idUser, Model model){
        model.addAttribute("insurance", new RealEstateInsurance());
        model.addAttribute("insurer", system.getPerson(idUser));
        model.addAttribute("idUser", idUser);
        model.addAttribute("allRealEstates", RealEstateType.values());
        model.addAttribute("allAnswers", Answer.values());
        return new ModelAndView("insurance/add/addRealEstate");
    }

    @PostMapping("/add/realEstate/toUser/{idUser}")
    public ModelAndView addRealEstateSubmit(@PathVariable int idUser, @ModelAttribute @Valid RealEstateInsurance insurance, BindingResult bindingResult, Model model ){
        if(bindingResult.hasErrors())
            return addRealEstateForm(idUser, model);
        if(!insurance.getAddress().isInitialized())
            return addRealEstateForm(idUser, model);

        addSubmit(insurance, idUser);

        int id = insurance.getID();
        return new ModelAndView("redirect:/insurance/id/" + id);
    }
}
