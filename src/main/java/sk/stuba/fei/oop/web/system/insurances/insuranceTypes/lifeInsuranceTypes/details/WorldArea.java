package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum WorldArea {
    SLOVAKIA,
    WORLD,
    WORLD_SLOVAKIA;

    //****** Reverse Lookup Implementation************//

    private static final Map<String, WorldArea> lookup = new HashMap<String, WorldArea>();

    static {
        for(WorldArea worldArea : WorldArea.values())
        {
            lookup.put(worldArea.name(), worldArea);
        }
    }

    public static WorldArea getEnum(String worldArea) throws NullPointerException {
        if(worldArea == null)
            throw new NullPointerException("Neexistujuci string.");

        return lookup.get(worldArea.toUpperCase());
    }

    public static ArrayList<WorldArea> getEnumsWithStarting(WorldArea area) throws NullPointerException{
        if(area == null)
            throw new NullPointerException("Neexistujuci enum.");

        ArrayList<WorldArea> enums = new ArrayList<WorldArea>(Arrays.asList(values()));
        enums.remove(area);
        enums.add(0, area);

        return enums;
    }
}
