package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.LifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.EuropeUnionArea;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.TravelPurpose;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TravelInsurance extends LifeInsurance {
    private final String type = "Travel";
    @NotNull
    private EuropeUnionArea areaCoverage;
    @NotNull
    private TravelPurpose travelPurpose;

    public TravelInsurance(){
        super();
    }

    @Override
    public boolean isNotInitialized() {
        return super.isNotInitialized() || areaCoverage == null || travelPurpose == null;
    }

    @Override
    public String toString() {
        return super.toString() + "\ncoverage: " + areaCoverage + "\ntravel purpose: " + travelPurpose;
    }
}
