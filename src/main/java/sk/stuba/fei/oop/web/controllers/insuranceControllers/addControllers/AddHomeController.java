package sk.stuba.fei.oop.web.controllers.insuranceControllers.addControllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.HomeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.RealEstateType;

import javax.validation.Valid;

@Controller
@RequestMapping("/insurance")
public class AddHomeController extends AddInsuranceController{
    @GetMapping("/add/home/toUser/{idUser}")
    public ModelAndView addHomeForm(@PathVariable int idUser, Model model){
        model.addAttribute("insurance", new HomeInsurance());
        model.addAttribute("insurer", system.getPerson(idUser));
        model.addAttribute("idUser", idUser);
        model.addAttribute("allRealEstates", RealEstateType.values());
        return new ModelAndView("insurance/add/addHome");
    }

    @PostMapping("/add/home/toUser/{idUser}")
    public ModelAndView addAccidentSubmit(@PathVariable int idUser, @ModelAttribute @Valid HomeInsurance insurance, BindingResult bindingResult, Model model ){
        if(bindingResult.hasErrors())
            return addHomeForm(idUser, model);
        if(!insurance.getAddress().isInitialized())
            return addHomeForm(idUser, model);

        addSubmit(insurance, idUser);

        int id = insurance.getID();
        return new ModelAndView("redirect:/insurance/id/" + id);
    }
}
