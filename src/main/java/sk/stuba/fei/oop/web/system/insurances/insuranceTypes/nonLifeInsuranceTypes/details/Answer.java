package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details;

import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.EuropeUnionArea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Answer {
    YES,
    NO;

    //****** Reverse Lookup Implementation************//

    private static final Map<String, Answer> lookup = new HashMap<String, Answer>();

    static {
        for(Answer answer : Answer.values())
        {
            lookup.put(answer.name(), answer);
        }
    }

    public static Answer getEnum(String answer) throws NullPointerException {
        if(answer == null)
            throw new NullPointerException("Neexistujuci string.");

        return lookup.get(answer.toUpperCase());
    }

    public static ArrayList<Answer> getEnumsWithStarting(Answer answer) throws NullPointerException{
        if(answer == null)
            throw new NullPointerException("Neexistujuci enum.");

        ArrayList<Answer> enums = new ArrayList<Answer>(Arrays.asList(values()));
        enums.remove(answer);
        enums.add(0, answer);

        return enums;
    }
}
