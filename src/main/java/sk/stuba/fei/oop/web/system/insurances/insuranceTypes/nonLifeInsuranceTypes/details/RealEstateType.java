package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details;

import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.EuropeUnionArea;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.RealEstateInsurance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum RealEstateType{
    FLAT,
    BRICK_HOUSE,
    WOODEN_HOUSE;

    //****** Reverse Lookup Implementation************//

    private static final Map<String, RealEstateType> lookup = new HashMap<String, RealEstateType>();

    static {
        for(RealEstateType type : RealEstateType.values())
        {
            lookup.put(type.name(), type);
        }
    }

    public static RealEstateType getEnum(String realEstateType) throws NullPointerException {
        if(realEstateType == null)
            throw new NullPointerException("Neexistujuci string.");

        return lookup.get(realEstateType.toUpperCase());
    }

    public static ArrayList<RealEstateType> getEnumsWithStarting(RealEstateType type) throws NullPointerException{
        if(type == null)
            throw new NullPointerException("Neexistujuci enum.");

        ArrayList<RealEstateType> enums = new ArrayList<RealEstateType>(Arrays.asList(values()));
        enums.remove(type);
        enums.add(0, type);

        return enums;
    }
}
