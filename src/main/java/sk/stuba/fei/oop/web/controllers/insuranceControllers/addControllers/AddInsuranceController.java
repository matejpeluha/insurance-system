package sk.stuba.fei.oop.web.controllers.insuranceControllers.addControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;


@Controller
abstract public class AddInsuranceController {
    @Autowired
    protected InsuranceSystem system;

    protected void addSubmit(Insurance insurance, int idUser){
        insurance.setID(system.getNextInsuranceID());
        insurance.setInsurerID(idUser);

        system.addInsurance(insurance);
    }
}
