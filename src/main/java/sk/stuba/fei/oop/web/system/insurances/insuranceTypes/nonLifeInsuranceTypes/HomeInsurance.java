package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.NonLifeInsurance;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Getter
public class HomeInsurance extends NonLifeInsurance {
    private final String type = "Home";
    @Min(100)
    private Integer homeDeviceValue;

    public HomeInsurance(){
        super();
    }


    @Override
    public boolean isNotInitialized() {
        return super.isNotInitialized() || homeDeviceValue == null;
    }

    @Override
    public String toString() {
        return super.toString() + "\ndevice value: " + homeDeviceValue;
    }
}
