package sk.stuba.fei.oop.web.controllers.insuranceControllers.changeControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.HomeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.RealEstateInsurance;


@Controller
@RequestMapping("/insurance")
public class ChangeInsuranceController {
    @Autowired
    protected InsuranceSystem system;

    @GetMapping("/change/{id}")
    public ModelAndView changeForm(@PathVariable int id){
        Insurance insurance = system.getInsurance(id);
        if(insurance instanceof TravelInsurance)
            return new ModelAndView("redirect:/insurance/change/travel/id/{id}");
        else if(insurance instanceof AccidentInsurance)
            return new ModelAndView("redirect:/insurance/change/accident/id/{id}");
        else if(insurance instanceof HomeInsurance)
            return new ModelAndView("redirect:/insurance/change/home/id/{id}");
        else if(insurance instanceof RealEstateInsurance)
            return new ModelAndView("redirect:/insurance/change/realEstate/id/{id}");
        else
            return new ModelAndView("redirect:/insurance/id/{id}");
    }

}
