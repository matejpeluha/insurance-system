package sk.stuba.fei.oop.web.controllers.insuranceControllers.changeControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.NonLifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.HomeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.RealEstateType;

@Controller
@RequestMapping("/insurance")
public class ChangeHomeController {
    @Autowired
    protected InsuranceSystem system;

    @GetMapping("/change/home/id/{id}")
    public ModelAndView changeHomeForm(@PathVariable int id, Model model){
        Insurance insurance = system.getInsurance(id);
        model.addAttribute("homeInsurance", (HomeInsurance)insurance);
        model.addAttribute("realEstateTypes", RealEstateType.getEnumsWithStarting(((NonLifeInsurance) insurance).getRealEstateType()));
        return new ModelAndView("insurance/change/changeHome");
    }

    @PostMapping("/change/home/{id}")
    public ModelAndView changeHomeSubmit(@PathVariable int id, @ModelAttribute HomeInsurance insurance, Model model, BindingResult bindingResult){
        insurance.setID(id);
        insurance = (HomeInsurance) system.setUnchangedParametersNonLifeInsurance(insurance);

        if(bindingResult.hasErrors())
            return changeHomeForm(id, model);

        if(insurance.isNotInitialized())
            return changeHomeForm(id, model);

        system.changeInsurance(insurance);

        return new ModelAndView("redirect:/insurance/id/{id}");
    }
}
