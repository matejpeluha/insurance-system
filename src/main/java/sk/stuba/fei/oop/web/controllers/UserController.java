package sk.stuba.fei.oop.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.userInfo.Person;
import javax.validation.Valid;


@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private InsuranceSystem system;

    @GetMapping("/")
    public ModelAndView findAll(Model model) {
        model.addAttribute("users", system.getAllPeople());
        return new ModelAndView("user/all");
    }

    @GetMapping("/id/{id}")
    public ModelAndView byId(@PathVariable int id, Model model) {
        Person  person = system.getPerson(id);

        model.addAttribute("user", person);
        model.addAttribute("insurances", system.getSignedInsurancesByUser(id));

        return new ModelAndView("user/one");
    }

    @GetMapping("/change/id/{id}")
    public ModelAndView changeUserForm(@PathVariable int id, Model model){
        Person person = system.getPerson(id);

        model.addAttribute("user", person);

        return new ModelAndView("user/change");
    }


    @PostMapping("/change/id/{id}")
    public ModelAndView changeUserSubmit(@PathVariable int id, @ModelAttribute Person person, Model model, BindingResult bindingResult){
        person.setID(id);
        person = system.setUnchangedParametersPerson(person);

        if(bindingResult.hasErrors())
            return changeUserForm(person.getID(), model);

        if(person.isNotInitialized())
                return changeUserForm(person.getID(), model);

        system.changePerson(person);

        return new ModelAndView("redirect:/user/id/{id}");
    }

    @GetMapping("/add")
    public ModelAndView addForm(Model model) {
        model.addAttribute("user", new Person());
        return new ModelAndView("user/add");
    }

    @PostMapping("/add")
    public ModelAndView addSubmit(@ModelAttribute @Valid Person person, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors())
            return addForm(model);

        person.setID(system.getNextUserID());

        if(person.isNotInitialized())
            return addForm(model);

        system.addPerson(person);

        int id = person.getID();
        return byId(id, model);
    }


}
