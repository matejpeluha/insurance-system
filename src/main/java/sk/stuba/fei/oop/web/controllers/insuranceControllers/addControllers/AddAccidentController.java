package sk.stuba.fei.oop.web.controllers.insuranceControllers.addControllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.WorldArea;

import javax.validation.Valid;

@Controller
@RequestMapping("/insurance")
public class AddAccidentController extends AddInsuranceController{
    @GetMapping("/add/accident/toUser/{idUser}")
    public ModelAndView addAccidentForm(@PathVariable int idUser, Model model){
        model.addAttribute("insurance", new AccidentInsurance());
        model.addAttribute("insurer", system.getPerson(idUser));
        model.addAttribute("idUser", idUser);
        model.addAttribute("allUsers", system.getAllPeople());
        model.addAttribute("allWorldAreas", WorldArea.values());
        return new ModelAndView("insurance/add/addAccident");
    }

    @PostMapping("/add/accident/toUser/{idUser}")
    public ModelAndView addAccidentSubmit(@PathVariable int idUser, @ModelAttribute @Valid AccidentInsurance insurance, BindingResult bindingResult, Model model ){
        if(bindingResult.hasErrors())
            return addAccidentForm(idUser, model);

        addSubmit(insurance, idUser);

        int id = insurance.getID();
        return new ModelAndView("redirect:/insurance/id/" + id);
    }
}
