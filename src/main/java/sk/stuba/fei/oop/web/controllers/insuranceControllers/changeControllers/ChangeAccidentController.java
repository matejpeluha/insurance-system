package sk.stuba.fei.oop.web.controllers.insuranceControllers.changeControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.LifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.WorldArea;

@Controller
@RequestMapping("/insurance")
public class ChangeAccidentController {
    @Autowired
    protected InsuranceSystem system;

    @GetMapping("/change/accident/id/{id}")
    public ModelAndView changeAccidentForm(@PathVariable int id, Model model){
        Insurance insurance = system.getInsurance(id);
        model.addAttribute("accidentInsurance", (AccidentInsurance)insurance);
        model.addAttribute("insured", system.getPerson(((LifeInsurance)insurance).getInsuredID()));
        model.addAttribute("allWorldAreas", WorldArea.getEnumsWithStarting(((AccidentInsurance) insurance).getWorldArea()));
        return new ModelAndView("insurance/change/changeAccident");
    }

    @PostMapping("/change/accident/{id}")
    public ModelAndView changeAccidentSubmit(@PathVariable int id, @ModelAttribute AccidentInsurance insurance, Model model, BindingResult bindingResult){
        insurance.setID(id);
        insurance = (AccidentInsurance) system.setUnchangedParametersLifeInsurance(insurance);

        if(bindingResult.hasErrors())
            return changeAccidentForm(id, model);

        if(insurance.isNotInitialized())
            return changeAccidentForm(id, model);

        system.changeInsurance(insurance);

        return new ModelAndView("redirect:/insurance/id/{id}");
    }


}
