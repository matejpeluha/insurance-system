package sk.stuba.fei.oop.web.system;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.LifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.NonLifeInsurance;
import sk.stuba.fei.oop.web.system.userInfo.Person;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

@Service
@Getter
public class InsuranceSystem {
    private ArrayList<Person> allPeople;
    private ArrayList<Insurance> allInsurances;
    @Autowired
    private DemoInsuranceSystemCreator systemCreator;

    public InsuranceSystem(){
        this.allPeople = new ArrayList<Person>();
        this.allInsurances = new ArrayList<Insurance>();
    }

    public ArrayList<Insurance> getSignedInsurancesByUser(int userID)  {
        ArrayList<Insurance> signedInsurances = new ArrayList<Insurance>();
        ArrayList<Integer> signedInsurancesID = getPerson(userID).getAllSignedInsurancesID();

        for(int insuranceID : signedInsurancesID) {
            Insurance insurance = getInsurance(insuranceID);
            signedInsurances.add(insurance);
        }

        return signedInsurances;
    }

    public void addTestPeople(){
        Person person1 = systemCreator.createTestPerson1();
        addPerson(person1);
        Person person2 = systemCreator.createTestPerson2();
        addPerson(person2);
    }

    public void addTestInsurance()  {
        Insurance insurance = systemCreator.createTestInsurance(allPeople.get(0));
        addInsurance(insurance);
    }

    public void sortAllInsurancesByID(){
        Collections.sort(this.allInsurances);
    }

    public void sortAllPeopleByID(){
        Collections.sort(this.allPeople);
    }

    public boolean isPersonIDUnique(int ID){
        for(Person person : allPeople){
            if(person.getID() == ID)
                return false;
        }

        return true;
    }

    public boolean isInsuranceIDUnique(int ID){
        for(Insurance insurance : allInsurances){
            if(insurance.getID() == ID)
                return false;
        }

        return true;
    }

    private boolean isInsurerInSystem(Person insurer){
        for(Person person : allPeople){
            if(person == insurer)
                return true;
        }

        return false;
    }

    public void addInsurance(Insurance insurance) throws NullPointerException, IllegalComponentStateException {
        if(insurance == null)
            throw new NullPointerException("Neexistujuca poistka");
        else if(insurance.isNotInitialized())
            throw new IllegalComponentStateException("Poistka nie je inicializovana.");

        getPerson(insurance.getInsurerID()).addInsurance(insurance);
        this.allInsurances.add(insurance);
    }

    public Insurance getInsurance(int ID) throws IllegalArgumentException {
        for(Insurance insurance : allInsurances){
            if(insurance.getID() == ID)
                return insurance;
        }

        throw new IllegalArgumentException("Poistka s id " + ID + " nie je zapisana v systeme.");
    }


    public void addPerson(Person person) throws NullPointerException, IllegalComponentStateException {
        if(person == null)
            throw new NullPointerException("Neexistujuca osoba");
        else if(person.isNotInitialized())
            throw new IllegalComponentStateException("Osoba nie je inicializovana.");

        this.allPeople.add(person);
        sortAllPeopleByID();
    }

    public Person getPerson(int ID) throws IllegalArgumentException {
        for(Person person : allPeople){
            if(person.getID() == ID)
                return person;
        }

        throw new IllegalArgumentException("Osoba s id " + ID + " nie je zapisana v systeme.");
    }

    public int getNextUserID(){
        if(allPeople.isEmpty())
            return 0;

        sortAllPeopleByID();

        int allPeopleSize = allPeople.size();
        Person lastPerson = allPeople.get(allPeopleSize - 1);

        return lastPerson.getID() + 1;
    }

    public int getNextInsuranceID(){
        if(allInsurances.isEmpty())
            return 0;

        sortAllInsurancesByID();

        int allInsurancesSize = allInsurances.size();
        Insurance lastInsurance = allInsurances.get(allInsurancesSize - 1);

        return lastInsurance.getID() + 1;
    }

    public Insurance setUnchangedParametersLifeInsurance(LifeInsurance insurance){
        Insurance oldInsurance = getInsurance(insurance.getID());
        setUnchangedParametersInsurance(insurance, oldInsurance);
        insurance.setInsuredID(((LifeInsurance)oldInsurance).getInsuredID());
        return insurance;
    }

    public Insurance setUnchangedParametersNonLifeInsurance(NonLifeInsurance insurance){
        Insurance oldInsurance = getInsurance(insurance.getID());
        setUnchangedParametersInsurance(insurance, oldInsurance);
        insurance.setAddress(((NonLifeInsurance)oldInsurance).getAddress());
        return insurance;
    }

    private Insurance setUnchangedParametersInsurance(Insurance insurance, Insurance oldInsurance){
        insurance.setInsurerID(oldInsurance.getInsurerID());
        insurance.setDateOfConclusionContract(oldInsurance.getDateOfConclusionContract());
        insurance.setInsurerID(oldInsurance.getInsurerID());

        return insurance;
    }



    public Person setUnchangedParametersPerson(Person person){
        Person oldPerson = getPerson(person.getID());

        person.setPIN(oldPerson.getPIN());

        person.setAllSignedInsurancesID(oldPerson.getAllSignedInsurancesID());

        if(person.getName().getFirstName().equals(""))
            person.setFirstName(null);
        else if(person.getName().getLastName().equals(""))
            person.setLastName(null);
        else if(person.getPermanentResidenceAddress().getStreet().equals(""))
            person.getPermanentResidenceAddress().setStreet(null);
        else if(person.getPermanentResidenceAddress().getMunicipality().equals(""))
            person.getPermanentResidenceAddress().setMunicipality(null);
        else if(person.getPermanentResidenceAddress().getPostCode().equals(""))
            person.getPermanentResidenceAddress().setPostCode(null);
        else if(person.getCorrespondenceAddress().getStreet().equals(""))
            person.getCorrespondenceAddress().setStreet(null);
        else if(person.getCorrespondenceAddress().getMunicipality().equals(""))
            person.getCorrespondenceAddress().setMunicipality(null);
        else if(person.getCorrespondenceAddress().getPostCode().equals(""))
            person.getCorrespondenceAddress().setPostCode(null);

        return person;
    }

    private void removePersonForReplace(int id){
        Person person = getPerson(id);
        this.allPeople.remove(person);
    }

    public void changePerson(Person person){
        removePersonForReplace(person.getID());

        this.getAllPeople().add(person);

        sortAllPeopleByID();
    }

    private void removeInsuranceForReplace(int id){
        Insurance insurance = getInsurance(id);
        this.allInsurances.remove(insurance);
    }

    public void changeInsurance(Insurance insurance){
        removeInsuranceForReplace(insurance.getID());

        this.getAllInsurances().add(insurance);

        sortAllInsurancesByID();
    }

    public void removeInsurance(int id){
        Insurance insurance = getInsurance(id);
        Person insurer = getPerson(insurance.getInsurerID());
        insurer.removeInsurance(id);
        allInsurances.remove(insurance);
    }

}
