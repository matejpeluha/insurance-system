package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes;


import lombok.Getter;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.NonLifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.Answer;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RealEstateInsurance extends NonLifeInsurance {
    private final String type = "Real Estate";
    @NotNull
    private Answer garageInsurance;

    public RealEstateInsurance(){
        super();
    }


    @Override
    public boolean isNotInitialized() {
        return super.isNotInitialized() || garageInsurance == null;
    }

    @Override
    public String toString() {
        return super.toString() + "\ngarage: " + garageInsurance;
    }
}
