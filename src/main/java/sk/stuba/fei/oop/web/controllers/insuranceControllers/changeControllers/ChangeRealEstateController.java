package sk.stuba.fei.oop.web.controllers.insuranceControllers.changeControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.InsuranceSystem;
import sk.stuba.fei.oop.web.system.insurances.Insurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.NonLifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.RealEstateInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.Answer;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.nonLifeInsuranceTypes.details.RealEstateType;

@Controller
@RequestMapping("/insurance")
public class ChangeRealEstateController {
    @Autowired
    protected InsuranceSystem system;

    @GetMapping("/change/realEstate/id/{id}")
    public ModelAndView changeRealEstateForm(@PathVariable int id, Model model){
        Insurance insurance = system.getInsurance(id);
        model.addAttribute("realEstateInsurance", (RealEstateInsurance)insurance);
        model.addAttribute("realEstateTypes", RealEstateType.getEnumsWithStarting(((NonLifeInsurance) insurance).getRealEstateType()));
        model.addAttribute("allGarageAnswers", Answer.getEnumsWithStarting(((RealEstateInsurance) insurance).getGarageInsurance()));
        return new ModelAndView("insurance/change/changeRealEstate");
    }

    @PostMapping("/change/realEstate/{id}")
    public ModelAndView changeRealEstateSubmit(@PathVariable int id, @ModelAttribute RealEstateInsurance insurance, Model model, BindingResult bindingResult){
        insurance.setID(id);
        insurance = (RealEstateInsurance) system.setUnchangedParametersNonLifeInsurance(insurance);

        if(bindingResult.hasErrors())
            return changeRealEstateForm(id, model);

        if(insurance.isNotInitialized())
            return changeRealEstateForm(id, model);

        system.changeInsurance(insurance);

        return new ModelAndView("redirect:/insurance/id/{id}");
    }

}
