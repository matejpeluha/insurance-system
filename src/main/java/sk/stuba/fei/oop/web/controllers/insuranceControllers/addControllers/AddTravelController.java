package sk.stuba.fei.oop.web.controllers.insuranceControllers.addControllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.EuropeUnionArea;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.TravelPurpose;

import javax.validation.Valid;

@Controller
@RequestMapping("/insurance")
public class AddTravelController extends AddInsuranceController{
    @GetMapping("/add/travel/toUser/{idUser}")
    public ModelAndView addTravelForm(@PathVariable int idUser, Model model){
        model.addAttribute("insurance", new TravelInsurance());
        model.addAttribute("insurer", system.getPerson(idUser));
        model.addAttribute("idUser", idUser);
        model.addAttribute("allUsers", system.getAllPeople());
        model.addAttribute("allAreaCoverages", EuropeUnionArea.values());
        model.addAttribute("allTravelPurposes", TravelPurpose.values());
        return new ModelAndView("insurance/add/addTravel");
    }

    @PostMapping("/add/travel/toUser/{idUser}")
    public ModelAndView addTravelSubmit(@PathVariable int idUser, @ModelAttribute @Valid TravelInsurance insurance, BindingResult bindingResult, Model model ){
        if(bindingResult.hasErrors())
            return addTravelForm(idUser, model);

        addSubmit(insurance, idUser);

        int id = insurance.getID();
        return new  ModelAndView("redirect:/insurance/id/" + id);
    }
}
