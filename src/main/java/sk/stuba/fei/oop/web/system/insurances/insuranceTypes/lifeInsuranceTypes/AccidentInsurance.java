package sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes;

import lombok.Getter;
import lombok.Setter;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.LifeInsurance;
import sk.stuba.fei.oop.web.system.insurances.insuranceTypes.lifeInsuranceTypes.details.WorldArea;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NotNull
public class AccidentInsurance extends LifeInsurance {
    private final String type = "Accident";
    @Min(100)
    private Integer amountForPermanentConsequences;
    @Min(100)
    private Integer amountForDeath;
    @Min(10)
    private Integer dailyAmountForHospitalization;
    private WorldArea worldArea;

    public AccidentInsurance(){
        super();
    }

    @Override
    public boolean isNotInitialized() {
        return super.isNotInitialized() || amountForDeath == null || amountForPermanentConsequences == null
                || dailyAmountForHospitalization == null || worldArea == null;
    }

    @Override
    public String toString() {
        return super.toString() + "\ndeath: " + amountForDeath + "\npermanent consequences: " + amountForPermanentConsequences
                + "\ndaily hospitalization: " + dailyAmountForHospitalization + "\ncoverage: " + worldArea;
    }
}
